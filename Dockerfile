FROM rust:1 as builder
WORKDIR /home/rust

COPY Cargo.toml .
COPY Cargo.lock .
COPY src src
RUN cargo build --release

FROM rust:1 as prod
WORKDIR /app

COPY --from=builder /home/rust/target/release/bigcollatzapi /app/bigcollatzapi
COPY www /app/www
CMD /app/bigcollatzapi
