
use bigcollatz::BigCollatz;
use warp::Filter;
use num_bigint::{RandBigInt};

use serde::{Serialize};
use num_traits::ToPrimitive;

#[derive(Serialize)]
struct XY{
    step: u32,
    length: u32,
}

#[derive(Serialize)]
struct Record{
    number : String,
    steps: usize,
    lengths :Vec<XY>,
}

fn generate_random_(size:u16) -> Record{
    let mut maxsize:u64 = u64::from( if size < 3000 {size} else {3000});
    if size > 3000 {
    maxsize = 3000;
    }

    let mut rng = rand::thread_rng();
    let ini = rng.gen_biguint(maxsize);
    let tmp = ini.clone();

    let c = BigCollatz::new(ini);
    let mut result :Vec<XY>= Vec::new();

    for x in c {
        result.push(XY{
            step:x.step,
            length:format!("{}",x.curr).len().to_u32().unwrap()
        } );
    }

    Record{
        number : format!("{}", tmp),
        steps: result.len(),
        lengths : result,
    }
}

async fn get_random(size:u16)
    -> Result<impl warp::Reply, warp::Rejection>{

    let record = generate_random_(size);

    Ok(warp::reply::json( &record ))
}

async fn list_random(size:u16, n:u16)
                    -> Result<impl warp::Reply, warp::Rejection>{

    let mut vec : Vec<Record> = Vec::new();
    for _ in 0..n{
        vec.push( generate_random_(size) );
    }

    Ok(warp::reply::json( &vec ))
}




#[tokio::main]
async fn main() {

    let get_collatz =warp::get()
        .and(warp::path("random"))
        .and(warp::path::param::<u16>())
        .and(warp::path::end())
        .and_then(get_random);

    let list_collatz =warp::get()
        .and(warp::path("list"))
        .and(warp::path::param::<u16>())
        .and(warp::path::param::<u16>())
        .and(warp::path::end())
        .and_then(list_random);

    let www = warp::path("static").and(warp::fs::dir("www/static"));

    let routes = warp::get().and(get_collatz).or(list_collatz).or(www);

    warp::serve(routes)
        .run(([0, 0, 0, 0], 3030))
        .await;
}
